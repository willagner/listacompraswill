class CreateCompras < ActiveRecord::Migration
  def change
    create_table :compras do |t|
      t.date :data
      t.text :description
      t.boolean :comprado
      t.references :responsavel, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
