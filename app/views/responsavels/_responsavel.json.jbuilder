json.extract! responsavel, :id, :name, :created_at, :updated_at
json.url responsavel_url(responsavel, format: :json)
